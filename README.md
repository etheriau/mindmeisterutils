# README #

This project contains a variety of utilities to import things into MindMeister and other mind mapping utilities, including XMind.

## Converter ##

The Converter allows you to convert mindmaps from one format to another.  Currently the best support is to convert a MindMeister file into pure HTML or Text representations.

To do so, run:

java ca.eyt.mindmeister.Converter --input [path to the above file] --output [path to the output file] -p HTML

The JSMIND output requires jmind.css and jmind.js from http://hizzgdev.github.io/jsmind/developer.html to be installed in the current directory. This will result in an open HTML file format that you can use on the web.

## Merge ##

The Merge tool allows you to merge a series of Mindmaps of one format into one file.

To do so, run:

java ca.eyt.mindmeister.Merge --input [path to the above file] --input [path to the above file] --output [path to the output file]

## pinboard.in ##

You can export your pinboard.in bookmarks as JSON and then export it into a .mind file which can be imported into MindMeister or any other Mind Map format.

Once in MindMeister, you can rebalance the mind map using the MindMeister's map

To get started, you would download your bookmarks in JSON format at https://pinboard.in/export/format:json/ .

Then run:

java ca.eyt.mindmeister.PinboardImport --input [path to the above file] --output [path to the output file]

There are a couple other formatting options which you can learn about by running the program without options.

## JIRA ##

You can export your JIRA tickets by doing a query, and in the upper right corner, select the Export button, and download the XML.

Once you have the XML, you can run:

java ca.eyt.mindmeister.JIRAMindMapper --input [path to the above file] --output [path to the output file]

By default,this will split it based on Epic. You can change this behaviour and other options which you can learn about by running the program without options.

## Who do I talk to? ##

* eric@eyt.ca
* https://twitter/etheriau
* https://www.eyt.ca/