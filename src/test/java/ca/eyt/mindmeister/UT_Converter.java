package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UT_Converter {
   @Test
   public void testMindMeisterToText() throws Exception {
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      Converter.convert( MindMapProvider.MindMeister, new File( getClass().getResource( "/Test_Mind_Map.mind" ).getFile() ),
              MindMapProvider.SimpleText, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "Test Mind Map\n" +
              "\tGeneral Topic\n" +
              "\t\tSubtopic\n" +
              "\tItem With Link\n" +
              "\tItem With Icon\n" +
              "\tItem with Color\n" +
              "\tItem with picture\n" +
              "\tFloater!\n", data );
   }

   @Test
   public void testMindMeisterToSimpleHtml() throws Exception {
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      Converter.convert( MindMapProvider.MindMeister, new File( getClass().getResource( "/Test_Mind_Map.mind" ).getFile() ),
              MindMapProvider.HTML, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "<html><head><title>Test Mind Map</title><meta charset='utf-8' /><link type='text/css' rel='stylesheet' href='style.css' /></head><body><ul><li>Test Mind Map</li><ul><li>General Topic</li><ul><li>Subtopic</li></ul><li><a href='https://www.eyt.ca'>Item With Link</a></li><li><img src='emoji/symbols-bangbang.png' alt='emoji/symbols-bangbang'/>Item With Icon</li><li>Item with Color</li><li><img src='filter.png' width='128' height='128'/>Item with picture</li></ul><li>Floater!</li></ul></body></html>", data );
   }

   @Test
   public void testMindMeisterToJsMind() throws Exception {
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      Converter.convert( MindMapProvider.MindMeister, new File( getClass().getResource( "/Test_Mind_Map.mind" ).getFile() ),
              MindMapProvider.JSMIND, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "<html><head><title>Test Mind Map</title><meta charset='utf-8' /><link type='text/css' rel='stylesheet' href='style.css' /><link type='text/css' rel='stylesheet' href='jsmind.css' /><script type='text/javascript' src='jsmind.js'></script></head><body><div id='jsmind_container'></div><script type='text/javascript'>var mind={'meta':{'name':'','author':'',version:''},'format':'node_array','data':[{\"topic\":\"Test Mind Map\",\"isroot\":true,\"id\":1045246785},{\"topic\":\"General Topic\",\"id\":1045246830,\"parentid\":1045246785},{\"topic\":\"Subtopic\",\"id\":1045246843,\"parentid\":1045246830},{\"topic\":\"Item With Link\",\"id\":1045246877,\"parentid\":1045246785},{\"topic\":\"Item With Icon\",\"id\":1045247080,\"parentid\":1045246785},{\"topic\":\"Item with Color\",\"id\":1045247131,\"parentid\":1045246785},{\"topic\":\"Item with picture\",\"id\":1045247199,\"background-image\":\"filter.png\",\"parentid\":1045246785},{\"topic\":\"Floater!\",\"id\":1045251697,\"parentid\":1045246785}]};jsMind.show({container:'jsmind_container',editable:false,theme:'primary'},mind);</script></body></html>", data );
   }

   @Test
   public void testMindMeisterToHTMLJsMind() throws Exception {
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      Converter.convert( MindMapProvider.MindMeister, new File( getClass().getResource( "/Test_Mind_Map.mind" ).getFile() ),
              MindMapProvider.HTMLJSMIND, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "<html><head><title>Test Mind Map</title><meta charset='utf-8' /><link type='text/css' rel='stylesheet' href='style.css' /><link type='text/css' rel='stylesheet' href='jsmind.css' /><script type='text/javascript' src='jsmind.js'></script></head><body><div id='jsmind_container'></div><script type='text/javascript'>var mind={'meta':{'name':'','author':'',version:''},'format':'node_array','data':[{\"topic\":\"Test Mind Map\",\"isroot\":true,\"id\":1045246785},{\"topic\":\"General Topic\",\"id\":1045246830,\"parentid\":1045246785},{\"topic\":\"Subtopic\",\"id\":1045246843,\"parentid\":1045246830},{\"topic\":\"Item With Link\",\"id\":1045246877,\"parentid\":1045246785},{\"topic\":\"Item With Icon\",\"id\":1045247080,\"parentid\":1045246785},{\"topic\":\"Item with Color\",\"id\":1045247131,\"parentid\":1045246785},{\"topic\":\"Item with picture\",\"id\":1045247199,\"background-image\":\"filter.png\",\"parentid\":1045246785},{\"topic\":\"Floater!\",\"id\":1045251697,\"parentid\":1045246785}]};jsMind.show({container:'jsmind_container',editable:false,theme:'primary'},mind);</script><ul><li>Test Mind Map</li><ul><li>General Topic</li><ul><li>Subtopic</li></ul><li><a href='https://www.eyt.ca'>Item With Link</a></li><li><img src='emoji/symbols-bangbang.png' alt='emoji/symbols-bangbang'/>Item With Icon</li><li>Item with Color</li><li><img src='filter.png' width='128' height='128'/>Item with picture</li></ul><li>Floater!</li></ul></body></html>", data );
   }
}
