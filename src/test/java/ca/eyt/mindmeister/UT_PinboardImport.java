package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import ca.eyt.mindmeister.library.Node;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UT_PinboardImport {
   @Test
   public void testPinboard() throws Exception {
      String data = Utils.slurpStream( getClass().getResourceAsStream( "/pinboard.json" ) ).toString();
      Node root = PinboardImport.buildMindMapFromPinboard( data );
      assertEquals( "Bookmarks", root.getTitle() );
      assertEquals( 2, root.getChildren().size() );
      assertEquals( "Bookmarks\n" +
              "\tMindMapping\n" +
              "\t\tMindMeisterUtils: Java Tools for MindMeister\n" +
              "\t\tMindMeister\n" +
              "\t\tjsMind - Developer & Open Source\n" +
              "\tJavaScript\n" +
              "\t\tjsMind - Developer & Open Source\n", root.toMap( MindMapProvider.SimpleText ) );
   }
}
