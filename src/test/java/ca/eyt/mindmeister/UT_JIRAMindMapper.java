package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UT_JIRAMindMapper {
   @Test
   public void testSimpleAssignee() throws Exception {
      File searchXml = new File( getClass().getResource( "/SearchRequest.xml" ).getFile() );
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      JIRAMindMapper.processJIRA( searchXml, JIRAMindMapper.OrganizeBy.assignee, MindMapProvider.SimpleText, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "Backlog\n" +
              "\tEric Theriault\n" +
              "\t\tCORE-42: Clean up Docker Images for only things we need to run with?\n" +
              "\t\tCORE-44: Upgrade JIRA\n", data );
   }

   @Test
   public void testEpic() throws Exception {
      File searchXml = new File( getClass().getResource( "/SearchRequest.xml" ).getFile() );
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      JIRAMindMapper.processJIRA( searchXml, JIRAMindMapper.OrganizeBy.epic, MindMapProvider.SimpleText, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "Backlog\n" +
              "\tUncategorized\n" +
              "\t\tCORE-42: Clean up Docker Images for only things we need to run with? (Eric Theriault)\n" +
              "\t\tCORE-44: Upgrade JIRA (Eric Theriault)\n", data );
   }

   @Test
   public void testSprint() throws Exception {
      File searchXml = new File( getClass().getResource( "/SearchRequest.xml" ).getFile() );
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      JIRAMindMapper.processJIRA( searchXml, JIRAMindMapper.OrganizeBy.sprint, MindMapProvider.SimpleText, tempFile, null );
      String data = Utils.slurpFile( tempFile ).toString();
      assertEquals( "Backlog\n" +
              "\tBacklog\n" +
              "\t\tCORE-42: Clean up Docker Images for only things we need to run with? (Eric Theriault)\n" +
              "\t\tCORE-44: Upgrade JIRA (Eric Theriault)\n", data );
   }

   @Test
   public void testBaseline() throws Exception {
      File searchXml = new File( getClass().getResource( "/SearchRequest.xml" ).getFile() );
      File baseline = File.createTempFile( getClass().getSimpleName(), ".test" );
      baseline.deleteOnExit();
      JIRAMindMapper.processJIRA( searchXml, JIRAMindMapper.OrganizeBy.assignee, MindMapProvider.MindMeister, baseline, null );

      searchXml = new File( getClass().getResource( "/SearchRequest-New.xml" ).getFile() );
      File tempFile = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile.deleteOnExit();
      JIRAMindMapper.processJIRA( searchXml, JIRAMindMapper.OrganizeBy.assignee, MindMapProvider.SimpleText, tempFile, baseline );

      File tempFile2 = File.createTempFile( getClass().getSimpleName(), ".test" );
      tempFile2.deleteOnExit();
      Converter.convert( MindMapProvider.MindMeister, tempFile,
              MindMapProvider.SimpleText, tempFile2, null );
      String data = Utils.slurpFile( tempFile2 ).toString();
      assertEquals( "Backlog\n" +
              "\tEric Theriault\n" +
              "\t\tCORE-44: Upgrade JIRA\n" +
              "\t\tCORE-46: Update Docker Image for new Libraries\n", data );
   }
}
