package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Font {
   @JsonProperty
   private int id;
   @JsonProperty
   private String name;
   @JsonProperty
   private String url;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName( String name ) {
      this.name = name;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl( String url ) {
      this.url = url;
   }
}
