package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public enum MindMapProvider {
   MindMeister,
   SimpleText,
   HTML,
   MARKDOWN,
   JSMIND,
   HTMLJSMIND;

   private static final Random rng = new Random();

   public static MindMapProvider[] getParsers() {
      return new MindMapProvider[] { MindMeister };
   }

   public void createFile( File file, Node node ) throws IOException {
      List<Node> nodesWithoutIds = new ArrayList<>();
      Set<Integer> nodeIds = new HashSet<>();
      node.visit( ( p, n ) -> {
         if ( n.getId() == 0 ) {
            nodesWithoutIds.add(n);
         }
         nodeIds.add( n.getId() );
      } );
      for ( Node n : nodesWithoutIds ) {
         int id = 0;
         while ( true ) {
            id = rng.nextInt();
            if ( id > 0 && nodeIds.add( id ) ) {
               break;
            }
         }
         n.setId( id );
      }
      switch ( this ) {
         case MindMeister:
            createMindMeisterFile( file, node );
            break;

         case SimpleText:
         case HTML:
         case MARKDOWN:
         case JSMIND:
         case HTMLJSMIND:
            try ( FileOutputStream fos = new FileOutputStream( file ) ) {
               fos.write( node.toMap( this ).getBytes( StandardCharsets.UTF_8.name() ) );
            }
            break;

         default:
            throw new UnsupportedOperationException( "Unknown provider: " + this );
      }
   }

   private void createMindMeisterFile( File file, Node node ) throws IOException {
      try ( ZipOutputStream fos = new ZipOutputStream( new FileOutputStream( file ) ) ) {
         ZipEntry entry = new ZipEntry( "map.json" );
         fos.putNextEntry( entry );

         MindMeisterFile map = new MindMeisterFile();
         map.setRoot( node );

         ObjectMapper objectMapper = new ObjectMapper();
         fos.write( objectMapper.writeValueAsString( map ).getBytes( StandardCharsets.UTF_8 ) );
         fos.closeEntry();
      }
   }

   public Node parse( File file, File pathForOutputImages ) throws IOException {
      if ( this == MindMapProvider.MindMeister ) {
         MindMeisterFile f = MindMeisterFile.parserFromFile( file, pathForOutputImages );
         return f.getRoot();
      } else {
         throw new UnsupportedOperationException( "Cannot parse file: " + this + " for " + file );
      }
   }
}
