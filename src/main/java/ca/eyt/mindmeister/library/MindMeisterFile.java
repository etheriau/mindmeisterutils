package ca.eyt.mindmeister.library;

import ca.eyt.mindmeister.Utils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@JsonIgnoreProperties( ignoreUnknown = true )
public class MindMeisterFile {
   @JsonProperty
   private String map_version = "2.6";
   @JsonProperty
   private Node root;
   @JsonProperty
   private List<Image> images;
   @JsonProperty
   private List<Attachment> attachments;
   @JsonProperty
   private List<Connection> connections;
   @JsonProperty //TODO: I think this one is wrong
   private Theme theme;
   @JsonProperty
   private int layout;

   public String getMapVersion() {
      return map_version;
   }

   public void setMapVersion( String map_version ) {
      this.map_version = map_version;
   }

   public Node getRoot() {
      return root;
   }

   public void setRoot( Node root ) {
      this.root = root;
   }

   public Image getImage( int id ) {
      if ( images == null ) {
         return null;
      }
      for ( Image img : images ) {
         if ( img.getId() == id ) {
            return img;
         }
      }
      return null;
   }

   public static MindMeisterFile parserFromFile( File file, File pathToStoreImages ) throws IOException {
      MindMeisterFile retval = null;
      try ( ZipInputStream in = new ZipInputStream( new FileInputStream( file ) ) ) {
         while ( true ) {
            ZipEntry entry = in.getNextEntry();
            if ( entry == null ) {
               break;
            }
            if ( entry.getName().equalsIgnoreCase( "map.json" ) ) {
               String data = Utils.slurpStream( in ).toString();
               ObjectMapper objectMapper = new ObjectMapper();
               objectMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
               retval = objectMapper.readValue( data, MindMeisterFile.class );
            } else if ( pathToStoreImages != null ) {
               if ( ! entry.getName().startsWith( "images/" ) || retval == null ) {
                  continue;
               }
               int id = Integer.parseInt( entry.getName().replace( "images/", "" ).replace( ".bin", "" ) );
               Image image = retval.getImage( id );
               if ( image == null ) {
                  continue;
               }
               File output = new File( pathToStoreImages, image.getFilename() );
               int i = 0;
               String filename = image.getFilename();
               while ( output.exists() ) {
                  String extension = filename.substring( filename.lastIndexOf( "." ) + 1 );
                  String f = filename.replace( "." + extension, "_" + ( ++ i ) + "." + extension );
                  image.setFilename( f );
                  output = new File( pathToStoreImages, f );
               }
               try ( FileOutputStream fos = new FileOutputStream( output ) ) {
                  Utils.copyStream( in, fos );
               }
            }
         }
      }
      if ( retval == null || retval.getRoot() == null ) {
         throw new FileNotFoundException( "Could not process file: " + file );
      }
      retval.getRoot().resolveImages( retval );
      return retval;
   }

   public void updateFile( File baselineFile, File outputFile ) throws IOException {
      try ( ZipOutputStream output = new ZipOutputStream( new FileOutputStream( outputFile ) ) ) {
         try ( ZipInputStream in = new ZipInputStream( new FileInputStream( baselineFile ) ) ) {
            while ( true ) {
               ZipEntry entry = in.getNextEntry();
               if ( entry == null ) {
                  break;
               }
               if ( entry.getName().equalsIgnoreCase( "map.json" ) ) {
                  ZipEntry entry2 = new ZipEntry( "map.json" );
                  output.putNextEntry( entry2 );

                  ObjectMapper objectMapper = new ObjectMapper();
                  output.write( objectMapper.writeValueAsString( this ).getBytes( StandardCharsets.UTF_8 ) );
                  output.closeEntry();
               } else {
                  ZipEntry entry2 = new ZipEntry( entry.getName() );
                  output.putNextEntry( entry2 );

                  Utils.copyStream( in, output );
                  output.closeEntry();
               }
            }
         }
      }
   }
}
