package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Theme {
   @JsonProperty
   private int id;
   @JsonProperty
   private String name;
   @JsonProperty
   private String root_selected_color;
   @JsonProperty
   private String root_children_selected_color;
   @JsonProperty
   private String nodes_selected_color;
   @JsonProperty
   private Style root_style;
   @JsonProperty
   private Style root_children_style;
   @JsonProperty
   private Style nodes_style;
   @JsonProperty
   private Background background;
   @JsonProperty
   private Line line;
   @JsonProperty
   private List<Style> styles;
   @JsonProperty
   private List<Boundary> boundary_styles;
   @JsonProperty
   private String thumbnail;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName( String name ) {
      this.name = name;
   }

   public String getRoot_selected_color() {
      return root_selected_color;
   }

   public void setRoot_selected_color( String root_selected_color ) {
      this.root_selected_color = root_selected_color;
   }

   public String getRoot_children_selected_color() {
      return root_children_selected_color;
   }

   public void setRoot_children_selected_color( String root_children_selected_color ) {
      this.root_children_selected_color = root_children_selected_color;
   }

   public String getNodes_selected_color() {
      return nodes_selected_color;
   }

   public void setNodes_selected_color( String nodes_selected_color ) {
      this.nodes_selected_color = nodes_selected_color;
   }

   public Style getRoot_style() {
      return root_style;
   }

   public void setRoot_style( Style root_style ) {
      this.root_style = root_style;
   }

   public Style getRoot_children_style() {
      return root_children_style;
   }

   public void setRoot_children_style( Style root_children_style ) {
      this.root_children_style = root_children_style;
   }

   public Style getNodes_style() {
      return nodes_style;
   }

   public void setNodes_style( Style nodes_style ) {
      this.nodes_style = nodes_style;
   }

   public Background getBackground() {
      return background;
   }

   public void setBackground( Background background ) {
      this.background = background;
   }

   public Line getLine() {
      return line;
   }

   public void setLine( Line line ) {
      this.line = line;
   }

   public List<Style> getStyles() {
      return styles;
   }

   public void setStyles( List<Style> styles ) {
      this.styles = styles;
   }

   public List<Boundary> getBoundary_styles() {
      return boundary_styles;
   }

   public void setBoundary_styles( List<Boundary> boundary_styles ) {
      this.boundary_styles = boundary_styles;
   }

   public String getThumbnail() {
      return thumbnail;
   }

   public void setThumbnail( String thumbnail ) {
      this.thumbnail = thumbnail;
   }
}
