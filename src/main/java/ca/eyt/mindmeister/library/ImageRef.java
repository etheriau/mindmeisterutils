package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImageRef {
   @JsonProperty
   private int id;
   @JsonProperty
   private String url;
   @JsonProperty
   private int image_file_id;
   @JsonProperty
   private int width;
   @JsonProperty
   private int height;

   private Image resolvedImage;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl( String url ) {
      this.url = url;
   }

   public int getImageFileId() {
      return image_file_id;
   }

   public void setImageFileId( int image_file_id ) {
      this.image_file_id = image_file_id;
   }

   public int getWidth() {
      return width;
   }

   public void setWidth( int width ) {
      this.width = width;
   }

   public int getHeight() {
      return height;
   }

   public void setHeight( int height ) {
      this.height = height;
   }

   public Image getResolvedImage() {
      return resolvedImage;
   }

   public void setResolvedImage( Image resolvedImage ) {
      this.resolvedImage = resolvedImage;
   }
}
