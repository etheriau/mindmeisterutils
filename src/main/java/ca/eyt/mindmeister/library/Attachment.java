package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attachment {
   @JsonProperty
   private long id;
   @JsonProperty
   private String filename;
   @JsonProperty
   private int size;
   @JsonProperty
   private String createdAtt;
   @JsonProperty
   private String conttentType;
   @JsonProperty
   private String api_url;

   public long getId() {
      return id;
   }

   public void setId( long id ) {
      this.id = id;
   }

   public String getFilename() {
      return filename;
   }

   public void setFilename( String filename ) {
      this.filename = filename;
   }

   public int getSize() {
      return size;
   }

   public void setSize( int size ) {
      this.size = size;
   }

   public String getCreatedAtt() {
      return createdAtt;
   }

   public void setCreatedAtt( String createdAtt ) {
      this.createdAtt = createdAtt;
   }

   public String getConttentType() {
      return conttentType;
   }

   public void setConttentType( String conttentType ) {
      this.conttentType = conttentType;
   }

   public String getApi_url() {
      return api_url;
   }

   public void setApi_url( String api_url ) {
      this.api_url = api_url;
   }
}
