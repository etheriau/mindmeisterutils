package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Style {
   @JsonProperty
   private int id;
   @JsonProperty
   private String name;
   @JsonProperty
   private List<Font> fonts;
   @JsonProperty
   private int fontSize;
   @JsonProperty
   private int boxStyle;
   @JsonProperty
   private String color;
   @JsonProperty
   private String backgroundColor;
   @JsonProperty
   private String borderColor;
   @JsonProperty
   private boolean bold;
   @JsonProperty
   private String italic;
   @JsonProperty
   private String gradient;
   @JsonProperty
   private boolean boxShadow;
   @JsonProperty
   private int borderWidth;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName( String name ) {
      this.name = name;
   }

   public List<Font> getFonts() {
      return fonts;
   }

   public void setFonts( List<Font> fonts ) {
      this.fonts = fonts;
   }

   public int getFontSize() {
      return fontSize;
   }

   public void setFontSize( int fontSize ) {
      this.fontSize = fontSize;
   }

   public int getBoxStyle() {
      return boxStyle;
   }

   public void setBoxStyle( int boxStyle ) {
      this.boxStyle = boxStyle;
   }

   public String getColor() {
      return color;
   }

   public void setColor( String color ) {
      this.color = color;
   }

   public String getBackgroundColor() {
      return backgroundColor;
   }

   public void setBackgroundColor( String backgroundColor ) {
      this.backgroundColor = backgroundColor;
   }

   public String getBorderColor() {
      return borderColor;
   }

   public void setBorderColor( String borderColor ) {
      this.borderColor = borderColor;
   }

   public boolean isBold() {
      return bold;
   }

   public void setBold( boolean bold ) {
      this.bold = bold;
   }

   public String getItalic() {
      return italic;
   }

   public void setItalic( String italic ) {
      this.italic = italic;
   }

   public String getGradient() {
      return gradient;
   }

   public void setGradient( String gradient ) {
      this.gradient = gradient;
   }

   public boolean isBoxShadow() {
      return boxShadow;
   }

   public void setBoxShadow( boolean boxShadow ) {
      this.boxShadow = boxShadow;
   }

   public int getBorderWidth() {
      return borderWidth;
   }

   public void setBorderWidth( int borderWidth ) {
      this.borderWidth = borderWidth;
   }
}
