package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Image {
   @JsonProperty
   private int id;
   @JsonProperty
   private String filename;
   @JsonProperty
   private int size;
   @JsonProperty
   private String createdAt;
   @JsonProperty
   private String contentType;
   @JsonProperty
   private String api_url;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getFilename() {
      return filename;
   }

   public void setFilename( String filename ) {
      this.filename = filename;
   }

   public int getSize() {
      return size;
   }

   public void setSize( int size ) {
      this.size = size;
   }

   public String getCreatedAt() {
      return createdAt;
   }

   public void setCreatedAt( String createdAt ) {
      this.createdAt = createdAt;
   }

   public String getContentType() {
      return contentType;
   }

   public void setContentType( String contentType ) {
      this.contentType = contentType;
   }

   public String getApiUrl() {
      return api_url;
   }

   public void setApiUrl( String api_url ) {
      this.api_url = api_url;
   }
}
