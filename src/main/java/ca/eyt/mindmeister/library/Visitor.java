package ca.eyt.mindmeister.library;

public interface Visitor {
   void visit( Node parent, Node node );
}
