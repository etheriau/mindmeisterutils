package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Line {
   @JsonProperty
   private String color;
   @JsonProperty
   private int style;
   @JsonProperty
   private int organicity;

   public String getColor() {
      return color;
   }

   public void setColor( String color ) {
      this.color = color;
   }

   public int getStyle() {
      return style;
   }

   public void setStyle( int style ) {
      this.style = style;
   }

   public int getOrganicity() {
      return organicity;
   }

   public void setOrganicity( int organicity ) {
      this.organicity = organicity;
   }
}
