package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Background {
   @JsonProperty
   private String image;
   @JsonProperty
   private String color;
   @JsonProperty
   private boolean repeat;

   public String getImage() {
      return image;
   }

   public void setImage( String image ) {
      this.image = image;
   }

   public String getColor() {
      return color;
   }

   public void setColor( String color ) {
      this.color = color;
   }

   public boolean isRepeat() {
      return repeat;
   }

   public void setRepeat( boolean repeat ) {
      this.repeat = repeat;
   }
}
