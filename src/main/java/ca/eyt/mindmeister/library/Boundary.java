package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Boundary {
   @JsonProperty
   private int id;
   @JsonProperty
   private String name;
   @JsonProperty
   private int shape;
   @JsonProperty
   private String fill_color;
   @JsonProperty
   private String stroke_color;
   @JsonProperty
   private int stroke_width;;
   @JsonProperty
   private int stroke_style;
   @JsonProperty
   private String fill_opacity;
   @JsonProperty
   private String stroke_opacity;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName( String name ) {
      this.name = name;
   }

   public int getShape() {
      return shape;
   }

   public void setShape( int shape ) {
      this.shape = shape;
   }

   public String getFill_color() {
      return fill_color;
   }

   public void setFill_color( String fill_color ) {
      this.fill_color = fill_color;
   }

   public String getStroke_color() {
      return stroke_color;
   }

   public void setStroke_color( String stroke_color ) {
      this.stroke_color = stroke_color;
   }

   public int getStroke_width() {
      return stroke_width;
   }

   public void setStroke_width( int stroke_width ) {
      this.stroke_width = stroke_width;
   }

   public int getStroke_style() {
      return stroke_style;
   }

   public void setStroke_style( int stroke_style ) {
      this.stroke_style = stroke_style;
   }

   public String getFill_opacity() {
      return fill_opacity;
   }

   public void setFill_opacity( String fill_opacity ) {
      this.fill_opacity = fill_opacity;
   }

   public String getStroke_opacity() {
      return stroke_opacity;
   }

   public void setStroke_opacity( String stroke_opacity ) {
      this.stroke_opacity = stroke_opacity;
   }
}
