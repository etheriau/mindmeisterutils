package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Connection {
  @JsonProperty
  private int id;
  @JsonProperty
  private int from_id;
  @JsonProperty
  private int to_id;
  @JsonProperty
  private int cp_from_x;
  @JsonProperty
  private int cp_from_y;
  @JsonProperty
  private int cp_to_x;
  @JsonProperty
  private int cp_to_y;
  @JsonProperty
  private int cp_scale_x;
  @JsonProperty
  private int cp_scale_y;
  @JsonProperty
  private String cp_zoom_factor;
  @JsonProperty
  private String color;
  @JsonProperty
  private String label;
}
