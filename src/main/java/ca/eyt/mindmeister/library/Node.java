package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Node {
   @JsonProperty
   private int id;
   @JsonProperty
   private String title;
   @JsonProperty
   private String note;
   @JsonProperty
   private String link;
   @JsonProperty
   private boolean floating = false;
   @JsonProperty
   private int rank = 1;
   @JsonProperty
   private List<Node> children;
   @JsonProperty
   private ImageRef image;
   @JsonProperty
   private List<Integer> pos;
   @JsonProperty
   private List<String> icon;
   @JsonProperty
   private String created_at;
   @JsonProperty
   private String updated_at;
   @JsonProperty
   private Task task;
   @JsonProperty
   private Style style;
   @JsonProperty
   private Boundary boundary;
   @JsonProperty //TODO: I think this one is wrong
   private List<String> attachments;
   @JsonProperty //TODO: I think this one is wrong
   private List<String> comments;
   @JsonProperty //TODO: I think this one is wrong
   private List<String> votes;

   public int getId() {
      return id;
   }

   public void setId( int id ) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle( String title ) {
      this.title = title;
   }

   public String getNote() {
      return note;
   }

   public void setNote( String note ) {
      this.note = note;
   }

   public String getLink() {
      return link;
   }

   public void setLink( String link ) {
      this.link = link;
   }

   public int getRank() {
      return rank;
   }

   public void setRank( int rank ) {
      this.rank = rank;
   }

   public void addChild( Node child ) {
      if ( children == null ) {
         children = new ArrayList<>();
      }
      children.add( child );
   }

   public void addChildren( Collection<Node> children ) {
      if ( children == null ) {
         children = new ArrayList<>();
      }
      children.addAll( children );
   }

   public void removeChild( Node child ) {
      if ( children == null ) {
         return;
      }
      children.remove( child );
   }

   public List<Node> getChildren() {
      return children;
   }

   public List<String> getIcon() {
      return icon;
   }

   public void addIcon( String icon ) {
      if ( this.image == null ) {
         this.icon = new ArrayList<>();
      }
      if ( ! this.icon.contains( icon ) ) {
         this.icon.add( icon );
      }
   }

   public String toMap( MindMapProvider type ) {
      switch ( type ) {
      case SimpleText:
         return toSimpleText( 0 );

      case HTML: {
         StringBuilder html = new StringBuilder();
         html.append( "<html>" );
         html.append( "<head>" );
         html.append( "<title>" ).append( title ).append( "</title>" );
         html.append( "<meta charset='utf-8' />" );
         html.append( "<link type='text/css' rel='stylesheet' href='style.css' />" );
         html.append( "</head>" );
         html.append( "<body>" );
         html.append( "<ul>" );
         html.append( toHTML() );
         html.append( "</ul>" );
         html.append( "</body>" );
         html.append( "</html>" );
         return html.toString();
      }

      case MARKDOWN: {
         StringBuilder html = new StringBuilder();
         html.append( toMarkDown( -1 ) );
         return html.toString();
      }

      case JSMIND:
      case HTMLJSMIND:
         {
         StringBuilder html = new StringBuilder();
         html.append( "<html>" );
         html.append( "<head>" );
         html.append( "<title>" ).append( title ).append( "</title>" );
         html.append( "<meta charset='utf-8' />" );
         html.append( "<link type='text/css' rel='stylesheet' href='style.css' />" );
         html.append( "<link type='text/css' rel='stylesheet' href='jsmind.css' />" );
         html.append( "<script type='text/javascript' src='jsmind.js'></script>" );
         html.append( "</head>" );
         html.append( "<body>" );
         html.append( "<div id='jsmind_container'></div>" );
         html.append( "<script type='text/javascript'>" +
                 "var mind={'meta':{'name':'','author':'',version:''},'format':'node_array','data':" ).append( toJSMind() ).append( "};" +
                 "jsMind.show({" +
                        "container:'jsmind_container'," +
                        "editable:false," +
                        "theme:'primary'" +
                     "},mind);" +
                 "</script>" );
         if ( type == MindMapProvider.HTMLJSMIND ) {
            html.append( "<ul>" );
            html.append( toHTML() );
            html.append( "</ul>" );
         }
         html.append( "</body>" );
         html.append( "</html>" );
         return html.toString();
      }

      default:
         throw new UnsupportedOperationException( "Unknown type: " + type );
      }
   }

   private String toSimpleText( int indent ) {
      StringBuilder retval = new StringBuilder();
      for ( int i = 0; i < indent; ++ i ) {
         retval.append( "\t" );
      }

      retval.append( title  ).append( "\n" );
      if ( children != null ) {
         for ( Node node : children ) {
            retval.append( node.toSimpleText( indent + 1 ) );
         }
      }
      return retval.toString();
   }

   private String toMarkDown( int indent ) {
      StringBuilder retval = new StringBuilder();
      for ( int i = 0; i < indent; ++ i ) {
         retval.append( "   " );
      }

      retval.append( indent == -1 ? "# " : "* " );
      if ( link != null ) {
         retval.append( "[" );
      }
      if ( image != null && image.getResolvedImage() != null ) {
         retval.append( "![" ).append( getTitle() ).append( "](./" ).append( image.getResolvedImage().getFilename() ).append( ") " );
      }
      retval.append( title.replace( "\r", " " ) );
      if ( link != null ) {
         retval.append( "](" ).append( link ).append( ")" );
      }
      retval.append( "\n" );
      if ( children != null ) {
         for ( Node node : children ) {
            retval.append( node.toMarkDown( indent + 1 ) );
         }
      }
      return retval.toString();
   }

   private String toHTML() {
      StringBuilder retval = new StringBuilder();
      retval.append( "<li>" );
      if ( link != null ) {
         retval.append( "<a href='" ).append( link ).append( "'>" );
      }
      if ( image != null && image.getResolvedImage() != null ) {
         retval.append( "<img src='" ).append( image.getResolvedImage().getFilename() ).append( "' width='" ).append( image.getWidth() ).append( "' height='" ).append( image.getHeight() ).append( "'/>" );
      }
      if ( icon != null ) {
         for ( String i : icon ) {
            //TODO: Get these images from somewhere?
            retval.append( "<img src='" ).append( i ).append( ".png' alt='" ).append( i ).append( "'/>" );
         }
      }
      retval.append( title  );
      if ( link != null ) {
         retval.append( "</a>" );
      }
      retval.append( "</li>" );
      if ( children != null ) {
         List<Node> filtered = new ArrayList<>( children );
         filtered.removeIf( e -> e.floating );
         handleHtmlNodes( filtered, false, retval );
         filtered = new ArrayList<>( children );
         filtered.removeIf( e -> ! e.floating );
         for ( Node node : filtered ) {
            handleHtmlNodes( Collections.singletonList( node ), true, retval );
         }
      }
      return retval.toString();
   }

   private static void handleHtmlNodes( List<Node> children, boolean floating, StringBuilder retval ) {
      if ( children.size() == 0 ) {
         return;
      }
      if ( ! floating ) {
         retval.append( "<ul>" );
      }
      for ( Node node : children ) {
         retval.append( node.toHTML() );
      }
      if ( ! floating ) {
         retval.append( "</ul>" );
      }
   }

   void resolveImages( MindMeisterFile file ) {
      if ( image != null ) {
         image.setResolvedImage( file.getImage( image.getImageFileId() ) );
         if ( image.getResolvedImage() == null ) {
            image = null;
         }
      }
      if ( children != null ) {
         for ( Node child : children ) {
            child.resolveImages( file );
         }
      }
   }


   private String toJSMind() {
      JSONArray array = new JSONArray();
      toJSMind( null, this, array );
      return array.toString();
   }

   /**
    * Converts this to a JSMind array
    * @link https://github.com/hizzgdev/jsmind
    */
   private void toJSMind( Node parent, Node node, JSONArray array ) {
      JSONObject obj = new JSONObject();
      array.put( obj );
      obj.put( "id", node.getId() );
      if ( parent == null ) {
         obj.put( "isroot", true );
      } else {
         obj.put( "parentid", parent.getId() );
      }
      obj.put( "topic", title );
      if ( image != null && image.getResolvedImage() != null ) {
         obj.put( "background-image", image.getResolvedImage().getFilename() );
      }
      if ( children != null ) {
         for ( Node child : children ) {
            child.toJSMind( this, child, array );
         }
      }
   }

   public void visit( Visitor visitor ) {
      visit( visitor, null );
   }

   private void visit( Visitor visitor, Node parent ) {
      visitor.visit( parent, this );
      if ( children != null ) {
         List<Node> childrenList = new ArrayList<>( children );
         for ( Node child : childrenList ) {
            child.visit( visitor, this );
         }
      }
   }
}
