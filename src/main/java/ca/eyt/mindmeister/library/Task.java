package ca.eyt.mindmeister.library;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Task {
   @JsonProperty
   private String from;
   @JsonProperty
   private String until;
   @JsonProperty
   private String resource;
   @JsonProperty
   private String effort;
   @JsonProperty
   private int notify;

   public String getFrom() {
      return from;
   }

   public void setFrom( String from ) {
      this.from = from;
   }

   public String getUntil() {
      return until;
   }

   public void setUntil( String until ) {
      this.until = until;
   }

   public String getResource() {
      return resource;
   }

   public void setResource( String resource ) {
      this.resource = resource;
   }

   public String getEffort() {
      return effort;
   }

   public void setEffort( String effort ) {
      this.effort = effort;
   }

   public int getNotify() {
      return notify;
   }

   public void setNotify( int notify ) {
      this.notify = notify;
   }
}
