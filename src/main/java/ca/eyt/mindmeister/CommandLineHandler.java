package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CommandLineHandler {
   private final Options options = new Options();
   private CommandLine commandLine;

   private final Option input;
   private final Option output;
   private final Option providerType;

   CommandLineHandler() {
      input = new Option( "i", "input", true, "Input file name" );
      input.setRequired( true );
      options.addOption( input );

      output = new Option( "o", "output", true, "Output file name" );
      output.setRequired( true );
      options.addOption( output );

      providerType = new Option( "p", "provider", true, "Mind Map Provider: " + Arrays.toString( MindMapProvider.values() ) );
      options.addOption( providerType );
   }

   public void parse( Class<?> clazz, String [] args ) {
      CommandLineParser parser = new DefaultParser();

      try {
         commandLine = parser.parse( options, args );
      } catch ( ParseException e ) {
         System.out.println( e.getMessage() );
         HelpFormatter formatter = new HelpFormatter();
         formatter.printHelp( clazz.getSimpleName(), options );

         System.exit( 1 );
      }
   }

   public File getInputFile() {
      File infile = new File( commandLine.getOptionValue( input.getLongOpt() ) );
      if ( !infile.exists() ) {
         System.err.println( "The source file does not exist: " + commandLine.getOptionValue( input.getLongOpt() ) );
         System.exit( 1 );
      }
      return infile;
   }

   public List<File> getInputFiles() {
      String [] files = commandLine.getOptionValues( input.getLongOpt() );
      List<File> retval = new ArrayList<>();
      for ( String file : files ) {
         File infile = new File( file );
         if ( !infile.exists() ) {
            System.err.println( "The source file does not exist: " + commandLine.getOptionValue( input.getLongOpt() ) );
            System.exit( 1 );
         }
         retval.add( infile );
      }
      return retval;
   }

   public File getOutputFile() {
      return new File( commandLine.getOptionValue( output.getLongOpt() ) );
   }

   public MindMapProvider getProviderType() {
      if ( commandLine.hasOption( providerType.getLongOpt() ) ) {
         try {
            return MindMapProvider.valueOf( commandLine.getOptionValue( providerType.getLongOpt() ) );
         } catch ( IllegalArgumentException iae ) {
            System.err.println( "Invalid provider type: " + commandLine.getOptionValue( providerType.getLongOpt() ) );
            System.exit( 1 );
         }
      }

      return MindMapProvider.MindMeister;
   }

   public Options getOptions() {
      return options;
   }

   public CommandLine getCommandLine() {
      return commandLine;
   }
}
