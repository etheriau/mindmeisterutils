package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import org.apache.commons.cli.Option;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class Utils {
   public static StringBuilder slurpFile( File infile ) throws IOException {
      try ( FileInputStream stream = new FileInputStream( infile ) ) {
         return slurpStream( stream );
      }
   }

   public static StringBuilder slurpStream( InputStream stream ) throws IOException {
      StringBuilder data = new StringBuilder();
      byte[] read = new byte[ 4096 ];
      while ( true ) {
         int len = stream.read( read );
         if ( len == -1 ) {
            break;
         }
         data.append( new String( read, 0, len, StandardCharsets.UTF_8.name() ) );
      }
      return data;
   }

   public static void copyStream( InputStream input, OutputStream output ) throws IOException {
      byte [] read = new byte[ 4096 ];
      while ( true ) {
         int len = input.read( read );
         if ( len == -1 ) {
            break;
         }
         output.write( read, 0, len );
      }
   }

   public static MindMapProvider getMindMapProvider( CommandLineHandler parser, Option fromProvider ) {
      MindMapProvider from = MindMapProvider.MindMeister;
      if ( parser.getCommandLine().hasOption( fromProvider.getLongOpt() ) ) {
         try {
            from = MindMapProvider.valueOf( parser.getCommandLine().getOptionValue( fromProvider.getLongOpt() ) );
         } catch ( IllegalArgumentException iae ) {
            System.err.println( "Invalid provider type: " + parser.getCommandLine().getOptionValue( fromProvider.getLongOpt() ) );
            System.exit( 1 );
         }
      }
      return from;
   }
}
