package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import ca.eyt.mindmeister.library.Node;
import org.apache.commons.cli.Option;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Merges two or more mindmaps into one -- the second one will lose its root node.
 */
public class Merge {
   public static void main( String[] args ) throws Exception {
      CommandLineHandler parser = new CommandLineHandler();

      Option fromProvider = new Option( "f", "fromProvider", true, "Mind Map Provider: " + Arrays.toString( MindMapProvider.getParsers() ) );
      parser.getOptions().addOption( fromProvider );

      parser.parse( Converter.class, args );

      MindMapProvider provider = parser.getProviderType();
      List<File> infile = parser.getInputFiles();
      File outfile = parser.getOutputFile();
      File directory = outfile;
      if ( ! directory.isDirectory() ) {
         directory = directory.getParentFile();
      }

      MindMapProvider from = Utils.getMindMapProvider( parser, fromProvider );

      merge( from, infile, provider, outfile, directory );
   }

   public static void merge( MindMapProvider from, List<File> infiles, MindMapProvider provider, File outfile, File directory ) throws IOException {
      Node root = null;
      for ( File file : infiles ) {
         Node r = from.parse( file, directory );
         if ( root == null ) {
            root = r;
         } else {
            root.addChildren( r.getChildren() );
         }
      }
      if ( root == null ) {
         System.err.println( "Could not parse files." );
         return;
      }

      provider.createFile( outfile, root );
   }
}
