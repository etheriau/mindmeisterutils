package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import ca.eyt.mindmeister.library.Node;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Generates a mindmeister file of your pinboard.in bookmarks
 *
 * Download your bookmarks from here:
 *
 * https://pinboard.in/export/format:json/
 */
public class PinboardImport {
   public static void main( String[] args ) throws Exception {
      CommandLineHandler parser = new CommandLineHandler();
      parser.parse( PinboardImport.class, args );

      MindMapProvider provider = parser.getProviderType();
      File infile = parser.getInputFile();
      File outfile = parser.getOutputFile();

      String data = Utils.slurpFile( infile ).toString();

      Node root = buildMindMapFromPinboard( data );

      provider.createFile( outfile, root );
   }

   public static Node buildMindMapFromPinboard( String data ) {
      Node root = new Node();
      root.setTitle( "Bookmarks" );
      root.setLink( "https://pinboard.in/" );

      JSONArray array = new JSONArray( data );
      Map<String, Node> categories = new HashMap<>();
      for ( int i = 0; i < array.length(); ++ i ) {
         JSONObject obj = array.getJSONObject( i );

         for ( String tag : obj.getString( "tags" ).split( " " ) ) {
            Node parent = categories.get( tag );
            if ( parent == null ) {
               parent = new Node();
               parent.setTitle( tag );
               root.addChild( parent );
               categories.put( tag, parent );
            }

            Node node = new Node();
            node.setLink( obj.getString( "href" ) );
            node.setTitle( obj.getString( "description" ) );
            if ( obj.has( "extended" ) ) {
               node.setNote( obj.getString( "extended" ) );
            }
            parent.addChild( node );
         }
      }
      return root;
   }
}
