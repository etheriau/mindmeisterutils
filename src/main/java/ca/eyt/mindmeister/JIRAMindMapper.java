package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import ca.eyt.mindmeister.library.MindMeisterFile;
import ca.eyt.mindmeister.library.Node;
import org.apache.commons.cli.Option;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Produces a MindMap based on your JIRA export.
 *
 * Do a query, and from top left, choose Export -> XML.  This will be the input file (SearchRequest.xml)
 */
public class JIRAMindMapper {
   enum OrganizeBy {
      epic,
      sprint,
      assignee
   }

   public static void main( String[] args ) throws Exception {
      CommandLineHandler parser = new CommandLineHandler();

      Option organizeByOption = new Option( "s", "organizeBy", true, "Organize By: " + Arrays.toString( OrganizeBy.values() ) );
      parser.getOptions().addOption( organizeByOption );
      Option baselineOption = new Option( "b", "baseline", true, "A copy of a MindMeister file that you wish to update, for example, for the next sprint. This will attempt to keep your structure the same for existing stuff, but change it for new things." );
      parser.getOptions().addOption( baselineOption );

      parser.parse( JIRAMindMapper.class, args );

      MindMapProvider provider = parser.getProviderType();
      File infile = parser.getInputFile();
      File outfile = parser.getOutputFile();
      File baselineFile = parser.getCommandLine().hasOption(  baselineOption.getLongOpt() ) ? new File( parser.getCommandLine().getOptionValue( baselineOption.getLongOpt() ) ) : null;
      if ( baselineFile != null && !baselineFile.exists() ) {
         System.err.println( "The baseline file does not exist: " + parser.getCommandLine().getOptionValue( baselineOption.getLongOpt() ) );
         System.exit( 1 );
      }

      OrganizeBy organizeBy = OrganizeBy.epic;
      if ( parser.getCommandLine().hasOption( organizeByOption.getLongOpt() ) ) {
         try {
            organizeBy = OrganizeBy.valueOf( parser.getCommandLine().getOptionValue( organizeByOption.getLongOpt() ) );
         } catch ( IllegalArgumentException iae ) {
            System.err.println( "Invalid provider type: " + parser.getCommandLine().getOptionValue( organizeByOption.getLongOpt() ) );
            System.exit( 1 );
         }
      }

      processJIRA( infile, organizeBy, provider, outfile, baselineFile );
   }

   public static void processJIRA( File infile, OrganizeBy organizeBy, MindMapProvider provider, File outfile, File baselineFile ) throws IOException {
      MindMeisterFile baseline = null;
      Map<String, Node> baselineNodes = new HashMap<>();
      if ( baselineFile != null ) {
         baseline = MindMeisterFile.parserFromFile( baselineFile, null );
      }
      if ( baseline != null ) {
         baseline.getRoot().visit( ( parent, child ) -> {
            if ( child.getTitle() != null ) {
               String [] parts = child.getTitle().split( ":" );
               if ( parts.length >1 ) {
                  baselineNodes.put( parts[0], child );
               }
            }
         } );
      }

      JSONObject obj = XML.toJSONObject( Utils.slurpFile( infile ).toString() );

      obj = obj.getJSONObject( "rss" );
      obj = obj.getJSONObject( "channel" );
      JSONArray array = obj.getJSONArray( "item" );
      Map<String, List<Node>> backlog = new HashMap<>();
      Map<String, Node> keyToValue = new HashMap<>();
      Set<String> epics = new HashSet<>();
      for ( int i = 0; i < array.length(); ++i ) {
         JSONObject entry = array.getJSONObject( i );

         String key = entry.getJSONObject( "key" ).getString( "content" );
         String summary = entry.getString( "summary" );
         String assignee = entry.getJSONObject( "assignee" ).getString( "content" );

         Node node = baselineNodes.remove( key );
         boolean wasInBaseLine = node != null;
         if ( node == null ) {
            node = new Node();
         }
         node.setLink( entry.getString( "link" ) );
         node.setTitle( key + ": " + summary + ( organizeBy != OrganizeBy.assignee ? ( " (" + assignee + ")" ) : "" ) );

         String epic = "";
         String sprint = "Backlog";
         JSONArray customfield = entry.has( "customfields" ) ? entry.getJSONObject( "customfields" ).optJSONArray( "customfield" ) : null;
         if ( customfield != null ) {
            for ( int j = 0; j < customfield.length(); ++j ) {
               JSONObject k = customfield.getJSONObject( j );
               if ( "com.pyxis.greenhopper.jira:gh-epic-link".equals( k.optString( "key" ) ) ) {
                  epic = k.getJSONObject( "customfieldvalues" ).getString( "customfieldvalue" );
                  epics.add( epic );
               }
               if ( "com.pyxis.greenhopper.jira:gh-sprint".equals( k.optString( "key" ) ) ) {
                  JSONObject s = k.getJSONObject( "customfieldvalues" ).optJSONObject( "customfieldvalue" );
                  if ( s != null ) {
                     sprint = s.getString( "content" );
                  }
               }
            }
         }
         // Add the calendar icon if the the epic is not backlog OR the status is in-progress.
         if ( ! sprint.equals( "Backlog" ) || entry.has( "status" ) && entry.getJSONObject( "status" ).getString( "content" ).equals( "In Progress" ) ) {
            node.addIcon( "calendar" );
         }

         if ( wasInBaseLine ) {
            continue;
         }

         String region;
         if ( organizeBy == OrganizeBy.epic ) {
            region = epic;
         } else if ( organizeBy == OrganizeBy.assignee ) {
            region = assignee;
         } else if ( organizeBy == OrganizeBy.sprint ) {
            region = sprint;
         } else {
            throw new UnsupportedOperationException( "Unknown organization: " + organizeBy );
         }

         List<Node> epicList = backlog.computeIfAbsent( region, k -> new ArrayList<>() );
         epicList.add( node );
         keyToValue.putIfAbsent( key, node );
      }

      // Remove all epics from the resulting set.
      for ( Map.Entry<String, List<Node>> entry : backlog.entrySet() ) {
         Iterator<Node> it = entry.getValue().iterator();
         while ( it.hasNext() ) {
            Node key = it.next();
            String k = key.getTitle().substring( 0, key.getTitle().indexOf( ":" ) );
            if ( epics.contains( k ) ) {
               it.remove();
            }
         }
      }

      Node root;
      if ( baseline != null ) {
         root = baseline.getRoot();
         root.visit( ( parent, child ) -> {
            if ( child.getTitle() != null ) {
               String [] parts = child.getTitle().split( ":" );
               if ( parts.length >1 ) {
                  if ( baselineNodes.containsKey( parts[0] ) ) {
                     parent.removeChild( child );
                  }
               }
            }
         } );
      } else {
         root = new Node();
         root.setTitle( "Backlog" );
      }

      for ( Map.Entry<String, List<Node>> entry : backlog.entrySet() ) {
         String key = entry.getKey();
         if ( key.isEmpty() ) {
            key = "Uncategorized";
         }
         Node s = keyToValue.get( key );
         boolean added = false;
         if ( s == null && baseline != null ) {
            for ( Node child : baseline.getRoot().getChildren() ) {
               if ( child.getTitle().equalsIgnoreCase( key ) ) {
                  s = child;
                  added = true;
                  break;
               }
            }
         }
         if ( s == null ) {
            s = new Node();
            s.setTitle( key );
         }
         if ( ! added ) {
            root.addChild( s );
         }

         for ( Node e : entry.getValue() ) {
            s.addChild( e );
         }
      }

      if ( baseline == null ) {
         provider.createFile( outfile, root );
      } else {
         baseline.updateFile( baselineFile, outfile );
      }
   }
}

