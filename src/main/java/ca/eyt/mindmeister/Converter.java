package ca.eyt.mindmeister;

import ca.eyt.mindmeister.library.MindMapProvider;
import ca.eyt.mindmeister.library.Node;
import org.apache.commons.cli.Option;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Converts a file from one file to another file.
 */
public class Converter {
   public static void main( String[] args ) throws Exception {
      CommandLineHandler parser = new CommandLineHandler();

      Option fromProvider = new Option( "f", "fromProvider", true, "Mind Map Provider: " + Arrays.toString( MindMapProvider.getParsers() ) );
      parser.getOptions().addOption( fromProvider );

      parser.parse( Converter.class, args );

      MindMapProvider provider = parser.getProviderType();
      File infile = parser.getInputFile();
      File outfile = parser.getOutputFile();
      File directory = outfile;
      if ( ! directory.isDirectory() ) {
         directory = outfile.getCanonicalFile().getParentFile();
      }

      MindMapProvider from = Utils.getMindMapProvider( parser, fromProvider );
      convert( from, infile, provider, outfile, directory );
   }

   public static void convert( MindMapProvider from, File infile, MindMapProvider provider, File outfile, File directory ) throws IOException {
      Node root = from.parse( infile, directory );

      provider.createFile( outfile, root );
   }
}
